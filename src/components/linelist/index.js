import React from "react";
import { Grid, Paper, Avatar } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import MoreVertIcon from "@material-ui/icons/MoreVert";

export default function Datas(props) {
  return (
    <Grid container xs={12} style={{ padding: "10px 10px" }}>
      {props.datas
        .slice(
          props.page * props.rowPerPage,
          props.page * props.rowPerPage + props.rowPerPage
        )
        .filter(
          (name) =>
            name.name.toLowerCase().includes(props.searchStr) ||
            name.location.toLowerCase().includes(props.searchStr) ||
            name.companyName.toLowerCase().includes(props.searchStr) ||
            name.email.toLowerCase().includes(props.searchStr)
        )
        .map((val, index) => (
          <Grid items key={index} xs={12} sm={6} md={4} lg={3}>
            <Grid style={{ position: "relative", top: 18, height: 0 }}>
              {" "}
              <IconButton
                aria-label="more"
                style={{
                  backgroundColor: "rgb(238 234 234)",
                  padding: 2,
                  position: "absolute",
                  right: 17,
                }}
                aria-controls="long-menu"
                aria-haspopup="true"
                onClick={(e) => {
                  props.handleClick(e);
                  props.handleSetId(val.id);
                }}
              >
                <MoreVertIcon />
              </IconButton>
            </Grid>
            <Menu
              id="long-menu"
              anchorEl={props.anchorEl}
              open={props.open}
              onClose={props.handleClose}
            >
              <MenuItem
                onClick={(e) => {
                  props.handleEditdata();
                }}
              >
                Edit
              </MenuItem>
              <MenuItem
                onClick={(e) => {
                  props.handleClickOpen(true);
                }}
              >
                Delete
              </MenuItem>
            </Menu>
            <Paper
              elevation={0}
              style={{ margin: "10px", padding: "20px", borderRadius: "10px" }}
            >
              <Avatar
                style={{
                  margin: "auto",
                  padding: "6px",
                }}
              >
                {" "}
                {val.name[0]}
              </Avatar>
              <Grid
                conatiner
                style={{
                  width: "100%",
                  padding: "10px 0px",
                }}
              >
                <Grid container>
                  <Grid
                    item
                    xs={6}
                    style={{ display: "flex" }}
                    justify="flex-end"
                  >
                    Emp Name :
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{
                      display: "flex",
                      color: "#9c27b0",
                      paddingLeft: "6px",
                      wordBreak: "break-all",
                    }}
                    justify="flex-start"
                  >
                    {val.name}
                  </Grid>
                </Grid>

                <Grid container style={{ paddingTop: "8px" }}>
                  <Grid
                    item
                    xs={6}
                    style={{ display: "flex" }}
                    justify="flex-end"
                  >
                    Company Name :
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{
                      display: "flex",
                      color: "#9c27b0",
                      paddingLeft: "6px",
                    }}
                    justify="flex-start"
                  >
                    {val.companyName}
                  </Grid>
                </Grid>
                <Grid
                  container
                  style={{
                    padding: "8px 0px 8px 0px",
                  }}
                >
                  <Grid
                    item
                    xs={6}
                    style={{ display: "flex" }}
                    justify="flex-end"
                  >
                    Address :
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{
                      display: "flex",
                      paddingLeft: "6px",
                      color: "#9c27b0",
                    }}
                    justify="flex-start"
                  >
                    {val.location}
                  </Grid>
                </Grid>
                <Grid
                  container
                  style={{
                    padding: "0px 0px 8px 0px",
                  }}
                >
                  <Grid
                    item
                    xs={6}
                    style={{ display: "flex" }}
                    justify="flex-end"
                  >
                    Email :
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{
                      display: "flex",
                      wordBreak: "break-all",
                      paddingLeft: "6px",
                      color: "#9c27b0",
                    }}
                    justify="flex-start"
                  >
                    {val.email}
                  </Grid>
                </Grid>
                <Grid
                  container
                  style={{
                    padding: "0px 0px 8px 0px",
                  }}
                >
                  <Grid
                    item
                    xs={6}
                    style={{ display: "flex" }}
                    justify="flex-end"
                  >
                    status :
                  </Grid>
                  <Grid
                    item
                    xs={6}
                    style={{
                      display: "flex",
                      wordBreak: "break-all",
                      paddingLeft: "6px",
                      color: "#9c27b0",
                    }}
                    justify="flex-start"
                  >
                    {val.status ? "Active" : "Inactive"}
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        ))}
    </Grid>
  );
}
