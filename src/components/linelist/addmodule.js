import React from "react";
import { Grid, Dialog, TextField, Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Switch from "@material-ui/core/Switch";

const useStyles = makeStyles((theme) => ({
  contain: {
    [theme.breakpoints.up("md")]: {
      width: "580px",
    },
  },
  location: {
    [theme.breakpoints.up("md")]: {
      padding: "0px 0px 0px 16px",
    },
  },
}));

export default function Datas(props) {
  const classes = useStyles();

  return (
    <Dialog
      open={props.open}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <Grid container style={{ padding: "20px" }} className={classes.contain}>
        <Grid container>
          <Grid item style={{ marginLeft: "auto", paddingBottom: "8px" }}>
            <Button
              variant="outlined"
              color="primary"
              onClick={() => props.handlecloseContainer()}
              style={{ textTransform: "capitalize", margin: "0px 8px 0px 0px" }}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              onClick={() => props.handleAddCribs()}
              style={{ textTransform: "capitalize" }}
            >
              {props.isEdit ? "Update" : "Add"}
            </Button>
          </Grid>
        </Grid>
        <TextField
          required
          id="standard-required"
          label="Emp Name"
          style={{ margin: "10px 0px" }}
          variant="outlined"
          value={props.title}
          onChange={(e) => props.handletextChange(e, "title")}
          defaultValue="Hello World"
          fullWidth
          error={props.error ? props.error.title : false}
          helperText={props.error ? props.error.title : ""}
        />
        <TextField
          required
          label="email"
          style={{ margin: "10px 0px" }}
          variant="outlined"
          value={props.email}
          onChange={(e) => props.handletextChange(e, "email")}
          fullWidth
          error={props.error ? props.error.email : false}
          helperText={props.error ? props.error.email : ""}
        />
        <Grid container xs={12}>
          <Grid item container xs={12} md={4}>
            <TextField
              required
              id="standard-required"
              label="Emp Id"
              style={{ margin: "10px 0px" }}
              variant="outlined"
              value={props.id}
              onChange={(e) => props.handletextChange(e, "id")}
              fullWidth
              type="number"
              error={props.error ? props.error.id : false}
              helperText={props.error ? props.error.id : ""}
            />
          </Grid>

          <Grid item container xs={12} md={8} className={classes.location}>
            <TextField
              required
              id="standard-required"
              label="Location"
              style={{ margin: "10px 0px" }}
              variant="outlined"
              value={props.location}
              onChange={(e) => props.handletextChange(e, "location")}
              fullWidth={true}
              error={props.error ? props.error.location : false}
              helperText={props.error ? props.error.location : ""}
            />
          </Grid>
        </Grid>
        <Grid container xs={12}>
          <Grid item container xs={12} md={8}>
            <TextField
              required
              id="standard-required"
              label="Company name"
              style={{ margin: "10px 0px" }}
              variant="outlined"
              value={props.companyName}
              onChange={(e) => props.handletextChange(e, "companyName")}
              fullWidth
              error={props.error ? props.error.companyName : false}
              helperText={props.error ? props.error.companyName : ""}
            />
          </Grid>
          <Grid
            item
            container
            xs={12}
            md={4}
            justify="center"
            alignItems="center"
          >
            <Typography
              variant="caption"
              color="textSecondary"
              style={{ padding: "0px 0px 0px 2px", fontSize: "16px" }}
            >
              Status
            </Typography>
            <Switch
              checked={props.status}
              onChange={() => props.handleStatus()}
              name="checkedA"
              inputProps={{ "aria-label": "secondary checkbox" }}
            />
          </Grid>
        </Grid>
      </Grid>
    </Dialog>
  );
}
