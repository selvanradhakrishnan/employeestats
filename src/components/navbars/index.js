import Navbar from './TopNavbar';
import SideMenu from './SideNavbar';

export {
    Navbar,
    SideMenu
}