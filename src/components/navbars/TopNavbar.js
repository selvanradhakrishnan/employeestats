import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Hidden from "@material-ui/core/Hidden";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
// import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import MoreVert from "@material-ui/icons/MoreVert";
import { Avatar, Drawer } from "@material-ui/core";
import { List, Tooltip } from "@material-ui/core";
import InvertColors from "@material-ui/icons/InvertColors";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PermIdentity from "@material-ui/icons/PermIdentity";
import Popover from "@material-ui/core/Popover";
import { withRouter } from "react-router-dom";
import SideMenu from "./SideNavbar";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { ThemeContext } from "../../contexts";

const theme1 = createMuiTheme({
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: "0.8em",
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  appBar: {
    width: "100%",
    height: 56,
    background: theme.palette.background.paper,
    color: theme.palette.primary.main,
    paddingTop: 8,
    boxShadow:
      "0px 1px 3px 0px rgba(0, 0, 0, 0.06), 0px 1px 1px 0px rgba(0, 0, 0, 0), 0px 2px 1px -1px rgba(0, 0, 0, 0)",
    [theme.breakpoints.down("sm")]: {
      paddingTop: 2,
    },
  },
  header: {
    display: "flex",
    paddingLeft: 12,
  },
  headerChild: {
    paddingLeft: 8,
    paddingTop: 2,
  },
  imgContainer: {
    display: "flex",
    textAlign: "center",
  },
  avatar: {
    backgroundColor: theme.palette.background.paper,
  },
  icon: {
    color: theme.palette.primary.dark,
    padding: 16,
  },
  addBtn: {
    position: "absolute",
    right: 40,
  },
  deskTopAddBtn: {
    position: "absolute",
    right: 35,
  },
  profilePicContainer: {
    textAlign: "-webkit-right",
    cursor: "pointer",
    paddingRight: 16,
  },
  profilePic: {
    "&:hover": {
      boxShadow: "1px 1px 5px black",
    },
  },
  themeIcon: {
    marginTop: "0px !important",
    marginBottom: "0px !important",
  },
}));

const profileIconOption = [
  {
    name: "Profile",
    icon: <PermIdentity />,
  },
];

const Navbar = (props) => {
  const classes = useStyles();
  const theme = React.useContext(ThemeContext);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [state, setState] = React.useState({ isOpen: false });

  const handleMoreProfileClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleProfilePopperClose = () => {
    setAnchorEl(null);
  };

  const toogleDrawer = (e) => {
    setState({ isOpen: false });
  };

  const open = Boolean(anchorEl);
  const id = open ? "logout-popover" : undefined;

  return (
    <AppBar position="static" className={classes.appBar}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Hidden smDown>
          <Grid item md={3} lg={2} xl={2} className={classes.header}>
            <Avatar
              src={
                "https://www.fastsalehomes.co.uk/resources/whiteblue/selling-your-home/red_house.v1540203386.png"
              }
            />
            <div className={classes.headerChild}>
              <Typography
                style={{ lineHeight: 1, fontSize: "1.1rem", fontWeight: "600" }}
              >
                Employee maintenance
              </Typography>
              <Typography
                variant="caption"
                color="textSecondary"
                style={{ padding: "0px 0px 0px 2px" }}
              >
                To maintain employee details
              </Typography>
            </div>
          </Grid>
        </Hidden>

        <Hidden mdUp>
          <Grid item container xs={6}>
            <IconButton
              aria-label="menu"
              onClick={() => setState({ isOpen: true })}
            >
              <MenuIcon color="primary" />
            </IconButton>
            <Typography
              style={{
                lineHeight: 1,
                fontSize: "1.1rem",
                fontWeight: "600",
                display: "flex",
                alignItems: "center",
              }}
            >
              Crib Hounds
            </Typography>
          </Grid>
        </Hidden>

        <Grid className={classes.deskTopAddBtn}>
          <MuiThemeProvider theme={theme1}>
            <Tooltip
              title="Dark/Lite mode"
              style={{ marginTop: 10, marginBottom: 10 }}
            >
              <IconButton
                className={classes.themeIcon}
                onClick={() => {
                  theme.setTheme({ ...theme, isDarkTheme: !theme.isDarkTheme });
                }}
              >
                <InvertColors />
              </IconButton>
            </Tooltip>
          </MuiThemeProvider>
        </Grid>

        <Hidden mdUp>
          <Grid item className={classes.addBtn}>
            {/* <IconButton
              aria-label="menu"
              onClick={() => props.history.push("/entry")}
            >
              <AddBox color="primary" />
            </IconButton> */}
          </Grid>
          <Grid item>
            <IconButton
              aria-label="menu"
              aria-describedby={id}
              onClick={handleMoreProfileClick}
            >
              <MoreVert color="primary" />
            </IconButton>
          </Grid>
        </Hidden>

        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleProfilePopperClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          <List component="nav" aria-label="main mailbox folders">
            {profileIconOption.map((option, index) => {
              return (
                <ListItem
                  key={index}
                  button
                  onClick={() => props.history.push(option.path)}
                >
                  <ListItemIcon style={{ minWidth: 36 }}>
                    {option.icon}
                  </ListItemIcon>
                  <ListItemText primary={option.name} />
                </ListItem>
              );
            })}
          </List>
        </Popover>

        <Drawer open={state.isOpen} onClose={() => setState({ isOpen: false })}>
          <SideMenu auth={props.auth} toogleDrawer={toogleDrawer} />
        </Drawer>
      </Grid>
    </AppBar>
  );
};

export default withRouter(Navbar);
