import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { NavLink, withRouter } from "react-router-dom";
// import DashBoard from '@material-ui/icons/DashboardOutlined';
import LineList from "@material-ui/icons/List";
import routes from "../../router/routes";

const useStyles = makeStyles((theme) => ({
  menuContainer: {
    backgroundImage: `linear-gradient(to bottom, ${theme.palette.primary.main} 0%, ${theme.palette.primary.light}  100%)`,
    color: theme.palette.secondary.main,
    minHeight: "calc( 100vh - 56px)",
    height: "100%",
    [theme.breakpoints.down("xs")]: {
      width: "calc(100vh - 16pc)",
    },
    [theme.breakpoints.down("sm")]: {
      width: "240px",
    },
  },
  linkStyle: {
    textDecoration: "none",
    color: theme.palette.common.white,
  },
  listContainer: {
    width: "100%",
  },
  iconsContainer: {
    minWidth: 40,
  },
  imageIcon: {
    width: "20px",
    height: "20px",
    fill: "#fff",
  },
  listItem: {
    "&:hover": {
      backgroundColor: theme.palette.primary.light,
    },
  },
  listItemActiveClass: {
    "& li": {
      backgroundColor: theme.palette.primary.dark,
    },
  },
}));

const SideMenu = (props) => {
  const classes = useStyles();

  const Menus = [
    {
      name: "Line List",
      icon: <LineList className={classes.imageIcon} />,
      path: routes.linelist,
    },
  ];

  return (
    <Grid container className={classes.menuContainer}>
      <Grid item className={classes.listContainer}>
        <List>
          {Menus.map((menu, index) => {
            return (
              <NavLink
                key={index}
                to={menu.path}
                className={classes.linkStyle}
                onClick={props.toogleDrawer}
                activeClassName={classes.listItemActiveClass}
              >
                <ListItem className={classes.listItem}>
                  <ListItemIcon className={classes.iconsContainer}>
                    {menu.icon}
                  </ListItemIcon>
                  <ListItemText
                    primary={menu.name}
                    primaryTypographyProps={{
                      variant: "subtitle2",
                    }}
                  />
                </ListItem>
              </NavLink>
            );
          })}
        </List>
      </Grid>
    </Grid>
  );
};

export default withRouter(SideMenu);
