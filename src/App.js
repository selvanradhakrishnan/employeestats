import React from 'react';

import AppAuth from './App.auth';
import AppAlert from './App.alert';
import AppGQLClient from './App.gql';
import AppTheme from './App.theme';
import RouterApp from './router';

function App() {
  return (
    <AppGQLClient>
      <AppAuth>
        <AppTheme >
          <AppAlert>
            <RouterApp />
          </AppAlert>
        </AppTheme>
      </AppAuth>
    </AppGQLClient>

  );
}

export default App;