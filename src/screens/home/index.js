import React from "react";
import Grid from "@material-ui/core/Grid";
import { Navbar, SideMenu } from "../../components";
import Hidden from "@material-ui/core/Hidden";
import { withRouter } from "react-router-dom";
import { AuthContext } from './../../contexts';
import withStyles from '@material-ui/core/styles/withStyles';
import Routes from '../../router/routes';

import {
    LineList,
} from '../'

const useStyles = theme => ({
    desktopNavbar: {
        width: "100%"
    },
    desktopSidebar: {
        width: 200
    },
    desktopSidebarContainer: {
        width: "100%"
    },
    appDesktopContainer: {
        marginLeft: 200
    },
    componentContainer: {
        width: "calc(100% - 200px)",
        backgroundColor: "#ebeff3",
        height: "calc(100vh - 56px)",
        // overflowY: "auto",
        paddingBottom: 16,
        [theme.breakpoints.down("sm")]: {
            width: "100%"
        }
    }
});

class Home extends React.Component {

    giveMeComponentToRender = (auth) => {
      if (this.props.match.path === Routes.linelist) {
            return <LineList auth={auth} {...this.props} />;
        } 
    };


    componentDidMount() {
    }

    toogleDrawer = () => false;

    render() {

        const { classes } = this.props;

        return (
            <Grid container direction="column" justify="center" alignItems="center">
                <Grid item sm={12} md={12} lg={12} className={classes.desktopNavbar}>
                    <Navbar
                        {...this.props}
                        auth={this.context}
                    />
                </Grid>
                <Grid item container className={classes.desktopSidebarContainer}>
                    <Hidden smDown>
                        <Grid item className={classes.desktopSidebar}>
                            <SideMenu auth={this.context} toogleDrawer={this.toogleDrawer} />
                        </Grid>
                    </Hidden>
                    <Grid item className={classes.componentContainer}>
                        {this.giveMeComponentToRender(this.context)}
                    </Grid>
                </Grid>
            </Grid>
        );
    }
};

Home.contextType = AuthContext;

export default withRouter(withStyles(useStyles)(Home));
