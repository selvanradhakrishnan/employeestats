import React from "react";
import SubHeader from "../../components/linelist/subHeader";
import { Grid } from "@material-ui/core";
import DataList from "../../components/linelist";
import AddModel from "../../components/linelist/addmodule";
import TablePagination from "@material-ui/core/TablePagination";
import DeletePopup from "./dialogPopup";
class LineList extends React.Component {
  constructor(props) {
    super();
    this.state = {
      data: [
        {
          id: "10",
          name: "Selvan Radhakrishnan",
          location: "Chennai",
          email: "selvan@gmail.com",
          companyName: "Crayond pvt ltd",
          status: true,
        },
        {
          id: "11",
          name: "Ms Dhoni",
          location: "Pondicherry",
          email: "dhonism@gmail.com",
          companyName: "Mediwave pvt ltd",
          status: true,
        },
        {
          id: "12",
          name: "Suresh raina",
          location: "Trichy",
          email: "raina@gmail.com",
          companyName: "DCKAP technologies",
          status: true,
        },
        {
          id: "13",
          name: "Jadeja",
          location: "Madurai",
          email: "jaddu@gmail.com",
          companyName: "crayond pvt ltd",
          status: true,
        },
        {
          id: "14",
          name: "Rama krish",
          location: "Kanyakumari",
          email: "krish@gmail.com",
          companyName: "Royal teck",
          status: true,
        },
        {
          id: "13",
          name: "Jadeja",
          location: "Madurai",
          email: "jaddu@gmail.com",
          companyName: "crayond pvt ltd",
          status: true,
        },
        {
          id: "14",
          name: "Vallarasu",
          location: "Kanyakumari",
          email: "krish@gmail.com",
          companyName: "Royal teck",
          status: true,
        },
        {
          id: "15",
          name: "Jayaraj",
          location: "Madurai",
          email: "jaddu@gmail.com",
          companyName: "crayond pvt ltd",
          status: true,
        },
        {
          id: "90",
          name: "Marcus",
          location: "Kanyakumari",
          email: "krish@gmail.com",
          companyName: "Royal teck",
          status: true,
        },
        {
          id: "16",
          name: "Jadeja",
          location: "Madurai",
          email: "jaddu@gmail.com",
          companyName: "crayond pvt ltd",
          status: true,
        },
        {
          id: "17",
          name: "Krish",
          location: "Kanyakumari",
          email: "krish@gmail.com",
          companyName: "Royal teck",
          status: true,
        },
        {
          id: "18",
          name: "Kumar",
          location: "Madurai",
          email: "jaddu@gmail.com",
          companyName: "crayond pvt ltd",
          status: true,
        },
        {
          id: "19",
          name: "Rama lal",
          location: "Kanyakumari",
          email: "krish@gmail.com",
          companyName: "Royal teck",
          status: true,
        },
        {
          id: "80",
          name: "Mugen",
          location: "Chennai",
          email: "krish@gmail.com",
          companyName: "Royal teck",
          status: true,
        },
      ],
      seperateId: "",
      anchorEl: null,
      searchStr: "",
      addCrib: false,
      image: "",
      status: true,
      companyName: "",
      email: "",
      title: "",
      location: "",
      id: "",
      error: {},
      unique: false,
      isEdit: false,
      commentIndex: "",
      currentPage: 0,
      rowPerPage: 12,
      dialogModel: false,
    };
  }
  handleSetId = (id) => {
    this.setState({ seperateId: id });
  };
  handleEditdata = () => {
    let data = this.state.data;
    var commentIndex = data.findIndex((c) => {
      return c.id === this.state.seperateId;
    });
    let editarray = this.state.data[commentIndex];
    this.setState({
      title: editarray.name,
      location: editarray.location,
      id: editarray.id,
      companyName: editarray.companyName,
      status: editarray.status,
      email: editarray.email,
      addCrib: true,
      isEdit: true,
      commentIndex,
      anchorEl: null,
    });
  };
  handleDelete = () => {
    let data = this.state.data;
    data = data.filter((val) => {
      return val.id !== this.state.seperateId;
    });
    this.setState({ data, anchorEl: null, dialogModel: false });
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };
  handleStatus = () => {
    this.setState({ status: !this.state.status });
  };
  handleClose = () => {
    this.setState({
      anchorEl: null,
    });
  };
  handleSetStr = (e) => {
    this.setState({ searchStr: e.target.value });
  };

  handleOpenContainer = () => {
    this.setState({
      addCrib: true,
      isEdit: false,
      commentIndex: "",
    });
  };
  handlecloseContainer = () => {
    this.setState({
      addCrib: false,
      image: "",
      title: "",
      location: "",
      id: "",
    });
  };
  uniquevalue = () => {
    let unique = false;
    let { error } = this.state;
    // eslint-disable-next-line
    this.state.data.map((val) => {
      if (val.id === this.state.id) {
        error["id"] = "Please enter unique Id";
        unique = true;
        return false;
      }
    });
    this.setState({ error, unique });
    return true;
  };
  handleregex = () => {
    let error = this.state.error;
    const regexProject = /^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/;
    if (regexProject.test(this.state.email)) {
      return true;
    } else {
      error["email"] = "Please enter the valid email";
      this.setState({ error });
      return false;
    }
  };
  handleAddCribs = () => {
    let { id, location, title, error, companyName, status, email } = this.state;
    if (!id) {
      error["id"] = "Please enter the Id";
      this.setState({ error });
    }
    if (!email) {
      error["email"] = "Please enter the email";
      this.setState({ error });
    }
    if (email) {
      this.handleregex();
    }
    if (!companyName) {
      error["companyName"] = "Please enter the company name";
      this.setState({ error });
    }
    if (!location) {
      error["location"] = "Please enter the Location";
      this.setState({ error });
    }
    if (!title) {
      error["title"] = "Please enter the Name";
      this.setState({ error });
    }

    if (id && location && title && companyName && email && this.handleregex()) {
      if (this.state.isEdit) {
        let { data } = this.state;
        data[this.state.commentIndex].name = this.state.title;
        data[this.state.commentIndex].id = this.state.id;
        data[this.state.commentIndex].location = this.state.location;
        data[this.state.commentIndex].status = this.state.status;
        data[this.state.commentIndex].companyName = this.state.companyName;
        data[this.state.commentIndex].email = this.state.email;
        this.setState({
          data,
          image: "",
          title: "",
          location: "",
          id: "",
          status: "",
          email: "",
          companyName: "",
          anchorEl: null,
          addCrib: false,
          error: {},
        });
      } else {
        let object = [
          {
            id: id,
            name: title,
            status: status,
            companyName: companyName,
            location: location,
            email: email,
          },
        ];
        let data = this.state.data;
        Array.prototype.push.apply(this.state.data, object);
        this.setState({
          data,
          anchorEl: null,
          addCrib: false,
          error: {},
          image: "",
          title: "",
          location: "",
          id: "",
          email: "",
          companyName: "",
        });
      }
    }
  };
  handleChangePage = (event, newPage) => {
    this.setState({ currentPage: newPage });
  };
  handleChangeRowsPerPage = (event) => {
    this.setState({
      rowPerPage: event.target.value,
      currentPage: 0,
    });
  };
  handletextChange = (e, name) => {
    this.setState({ [name]: e.target.value, error: {} });
  };
  handleClickOpen = (value) => {
    this.setState({ dialogModel: value });
  };
  render() {
    const open = Boolean(this.state.anchorEl);
    return (
      <Grid>
        <SubHeader
          handleOpenContainer={this.handleOpenContainer}
          handlecloseContainer={this.handlecloseContainer}
          searchStr={this.state.searchStr}
          handleSetStr={this.handleSetStr}
        />
        <Grid
          style={{
            height: "calc(100vh - 122px)",
            overflow: "auto",
          }}
          className="display-block"
        >
          <DataList
            handleOpenContainer={this.handleOpenContainer}
            handlecloseContainer={this.handlecloseContainer}
            searchStr={this.state.searchStr}
            datas={this.state.data}
            handleDelete={this.handleDelete}
            handleSetId={this.handleSetId}
            handleEditdata={this.handleEditdata}
            open={open}
            anchorEl={this.state.anchorEl}
            handleClick={this.handleClick}
            handleClose={this.handleClose}
            rowPerPage={this.state.rowPerPage}
            page={this.state.currentPage}
            handleClickOpen={this.handleClickOpen}
          />
          <AddModel
            companyName={this.state.companyName}
            status={this.state.status}
            location={this.state.location}
            title={this.state.title}
            open={this.state.addCrib}
            isEdit={this.state.isEdit}
            handleStatus={this.handleStatus}
            email={this.state.email}
            id={this.state.id}
            handleAddCribs={this.handleAddCribs}
            handletextChange={this.handletextChange}
            handlecloseContainer={this.handlecloseContainer}
            error={this.state.error}
          />
          <TablePagination
            rowsPerPageOptions={[12, 40, 100]}
            component="div"
            count={this.state.data.length}
            rowsPerPage={this.state.rowPerPage}
            page={this.state.currentPage}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
          <DeletePopup
            open={this.state.dialogModel}
            handleDelete={this.handleDelete}
            handleClickOpen={this.handleClickOpen}
          />
        </Grid>
      </Grid>
    );
  }
}

export default LineList;
