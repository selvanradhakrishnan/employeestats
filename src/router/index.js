import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import routes from "./routes"; //all routes

//Screens
import { 
    Home
 } from "./../screens";
 
export const RouterApp = (props) => {

    return (
        <Router>
            <Switch>
                <Redirect exact path="/" to={routes.linelist} />
                <Route exact component={Home} path={routes.linelist} {...props}/>
                {/* <Route path="*" component={NotFround} /> */}
            </Switch>
        </Router>
    )
}

export default RouterApp;
