import React from 'react';
import { AlertSnackBar } from './components';
import { SnackBarContext } from './contexts'

class AppAlert extends React.Component {
    constructor(props){
        super(props);
        this.state={
            open: false,
            type: '',
            msg: '',
            vertical: 'top',
            horizontal: 'center',             
        }
    }

    close = () =>{
        this.setState({
            open: false,
            type: '',
            msg: '',
            vertical: 'top',
            horizontal: 'center',
        })
    }

    set=(props)=>{
        this.setState({...props})
    }
    
    render(){
    return (
        <SnackBarContext.Provider value={{...this.state, onclose: this.close,
            setSnack:this.set }} >        
            {this.state.open ? <AlertSnackBar {...this.state} onclose={this.close} />: ''}
            {this.props.children}        
        </SnackBarContext.Provider>
    )
    }
}

export default AppAlert;