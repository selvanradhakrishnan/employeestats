import React from 'react';
// import axios from 'axios';
import { AuthContext } from './contexts';
// import config from './config';
import { withApollo } from '@apollo/react-hoc';


const AppAuth = (props) => {
    
    const [auth, setAuth] = React.useState({ user: {}, lasteupdate: '' });
    const [
        state, 
        // setState
    ] = React.useState({ isRefreshed: true });

    // Todo: Your Referesh API here

    // React.useEffect(() => {
    //     const fetchData = async () => {
    //         await axios.post(`${config.api_url}user/refresh`,{
    //             token:localStorage.getItem("auth_token")
    //         }).then(async (response) => {
    //             let token = response.data.token;                    
    //             axios.defaults.headers.common['Authorization'] = token;   

                
    //             localStorage.setItem("auth_token", token);
                
    //             setAuth({ user: giveMeUserData()});
    //             setState({ isRefreshed: true });
    //         }).catch((err) => {
    //             localStorage.clear();
    //             delete axios.defaults.headers.common['Authorization'];                
    //             setState({ isRefreshed: true });
    //         })
    //     }
    //     fetchData();  
    // }, [state.isRefreshed])

    return (
        <div>
            {
                state.isRefreshed ?
                    <AuthContext.Provider value={{ ...auth, setAuth }}>
                        {props.children}                       
                    </AuthContext.Provider>
                    : <div>Loading...</div>
            }
        </div>
 
    )
} 

export default withApollo(AppAuth);
